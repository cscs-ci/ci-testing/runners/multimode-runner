#!/usr/bin/env bash

set -e

SCRIPTDIR=$(dirname $(realpath "${BASH_SOURCE[0]:-$0}"))
source "$SCRIPTDIR"/common.sh

# exit when we run as root - this is not what we want
[[ $(id -u) == 0 ]] && echoerr "Running as root user" && exit $BUILD_FAILURE_EXIT_CODE
[[ ! -v SCRATCH ]] && echoerr "SCRATCH variable is empty. This is an error" && exit $BUILD_FAILURE_EXIT_CODE

# Early blocking of runner usage when project is not allowed to use this runner
ensure_runner_mode_allowed

if [[ ! -d "$SCRIPTDIR"/../modes/"$MODE" ]] ; then
    echoerr "Could not find runner for mode $MODE"
    exit $BUILD_FAILURE_EXIT_CODE
fi

mode_config=$([[ -x "$SCRIPTDIR"/../modes/"$MODE"/bin/config.sh ]] && "$SCRIPTDIR"/../modes/"$MODE"/bin/config.sh "$@")

multi_mode_config=$(cat << END
{
  "builds_dir": "${SCRATCH}/gitlab-runner/builds/${CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN}/${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}/${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}",
  "cache_dir": "${SCRATCH}/gitlab-runner/cache/${CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN}/${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}/${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}",
  "builds_dir_is_shared": false,
  "job_env": {
    "MODE": "$MODE"
  }
}
END
)

# Multi-mode config overwrites entries of mode_config. That's on purpose, we want to overwrite the builds_dir/cache_dir, also we expose MODE as environment variable
echo "$mode_config" "$multi_mode_config" | jq -s '.[0] * .[1]'
