#!/usr/bin/env bash

set -e

SCRIPTDIR=$(dirname $(realpath "${BASH_SOURCE[0]:-$0}"))
if [[ -x "$SCRIPTDIR"/../modes/"$MODE"/bin/prepare.sh ]] ; then
    env -u MODE "$SCRIPTDIR"/../modes/"$MODE"/bin/prepare.sh "$@"
fi

