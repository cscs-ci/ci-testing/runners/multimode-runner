#!/usr/bin/env bash

set -e

SCRIPTDIR=$(dirname $(realpath "${BASH_SOURCE[0]:-$0}"))
if [[ -x "$SCRIPTDIR"/../modes/"$MODE"/bin/cleanup.sh ]] ; then
    env -u MODE "$SCRIPTDIR"/../modes/"$MODE"/bin/cleanup.sh "$@"
fi
