function echoerr { cat <<< "$@" 1>&2; }

function ensure_runner_mode_allowed {
    # check if we are allowed to execute the requested mode
    [[ ! -v CUSTOM_ENV_CSCS_CI_MW_URL ]] && CUSTOM_ENV_CSCS_CI_MW_URL="https://cicd-ext-mw.cscs.ch/ci"
    local ALLOWED_MODE=$(curl --silent "$CUSTOM_ENV_CSCS_CI_MW_URL/allowance/runner?token=$CUSTOM_ENV_CI_JOB_TOKEN&num_nodes=${CUSTOM_ENV_SLURM_JOB_NUM_NODES:-1}")
    local ALLOWED=$(echo "$ALLOWED_MODE" | jq --raw-output '.allowed' || echo "'$ALLOWED_MODE' is not a valid JSON string")
    MODE=$(echo "$ALLOWED_MODE" | jq --raw-output '.mode' || echo "invalid-mode")

    if [[ "$ALLOWED" != "YES" ]] ; then
        echoerr "Permission error. Reason: $ALLOWED"
        exit $BUILD_FAILURE_EXIT_CODE
    fi
}
