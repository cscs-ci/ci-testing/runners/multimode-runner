# Multimode GitLab-Runner

Modes are stored in the directory `modes/MY_MODE`, where `MY_MODE` must match the desired runner-tag
that will select that specific mode.
This multimode runner is the gateway to all modes, but it will request the CI-Ext middleware and
request for allowance. Not all projects get allowance on all possible runner modes.

If a project is allowed to run on `MY_MODE`, then this runner will only continue to dispatch the job
to the real custom gitlab runner executors.
The different modes of all other custom gitlab runner executors must follow the scheme that the entrypoints
are stored in `bin/config.sh`, `bin/prepare.sh`, `bin/run.sh` and `bin/cleanup.sh`.

The multimode runner will overwrite any specified builds/cache directories from the mode-runner (see [bin/config.sh](/bin/config.sh))
